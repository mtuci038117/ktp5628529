public class Task18 {
    public static String isStrangePair(String word1, String word2){
        if (word1.isEmpty() && word2.isEmpty()){ //обе строки не содержат символов
            return "true";
        }
        else if(word1.isEmpty() && !word2.isEmpty()){ //одна из строк не содержит символов
            return "false";
        }
        else if(!word1.isEmpty() && word2.isEmpty()){ //одна из строк не содержит символов
            return "false";
        }
        else {
            char c1_b = word1.charAt(0); //первый символ первой строки
            char c2_b = word2.charAt(0); //первый символ второй строки
            char c1_e = word1.charAt(word1.length() - 1); //последний символ первой строки
            char c2_e = word2.charAt(word2.length() - 1); //последний символ второй строки
            // первый символ первой строки соответствует последнему символу второй строки и
            // первый символ второй строки соотвествует последнему символу первой строки
            if (c1_b == c2_e && c2_b == c1_e) {
                return "true";
            } else {
                return "false";
            }
        }
        }

    public static void main(String[] args) {
        System.out.println("isStrangePair(\"ratio\", \"orator\") ➞ " + isStrangePair("ratio", "orator"));
        System.out.println("isStrangePair(\"sparkling\", \"groups\") ➞ " + isStrangePair("sparkling", "groups"));
        System.out.println("isStrangePair(\"bush\", \"hubris\") ➞ " + isStrangePair("bush", "hubris"));
        System.out.println("isStrangePair(\"\", \"\") ➞ " + isStrangePair("", ""));
    }
}
