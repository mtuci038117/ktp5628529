public class Task15 {
    public static int getDecimalPlaces(String num){
        if (num.indexOf(".") != -1) { //проверка: в строке содержится "."
            String[] new_num; //создание пустого массива
            new_num = num.split("\\."); //добавление в пустой массив разделённую строку, "." - разделитель
            return new_num[1].length(); //вывод длины второго элемента массива (символы после ".")
        }
        else {
            return 0; //если в строке нет "."
        }
        }

    public static void main(String[] args) {
        System.out.println("getDecimalPlaces(\"43.20\") ➞ " + getDecimalPlaces("43.20"));
        System.out.println("getDecimalPlaces(\"400\") ➞ " + getDecimalPlaces("400"));
        System.out.println("getDecimalPlaces(\"3.1\") ➞ " + getDecimalPlaces("3.1"));
    }
}
