public class Task9 {
    public static int sumOfCubes(int[] nums){
        int sum = 0;
        for (int i = 0; i < nums.length; i++){ //перебор элементов массива
            sum = sum + nums[i] * nums[i] * nums[i]; //суммирование кубов каждого элемента
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println("sumOfCubes([3, 4, 5]) ➞ " + sumOfCubes(new int[]{3, 4, 5}));
        System.out.println("sumOfCubes([2]) ➞ " + sumOfCubes(new int[]{2}));
        System.out.println("sumOfCubes([]) ➞ " + sumOfCubes(new int[]{}));
    }
}
