public class Task4 {
    public static String profitableGamble(double prob, int prize, int pay){
        if (prob * prize > pay) //проверка prob * prize > pay
        {
            return "true";
        }
        else
        {
            return "false";
        }
    }

    public static void main(String[] args) {
        System.out.println("profitableGamble(0.2, 50, 9) ➞ " + profitableGamble(0.2, 50, 9));
        System.out.println("profitableGamble(0.9, 1, 2) ➞ " + profitableGamble(0.9, 1, 2));
        System.out.println("profitableGamble(0.9, 3, 2) ➞ " + profitableGamble(0.9, 3, 2));
    }
}
