public class Task20 {
    public static void hexLattice(int Z) {
        int N = 1; //количество точек для построение гексагона
        int k = 1; //индекс в ряде с количеством точек, начиная с 0.

        if (Z == 1){ //если вводимое количество точек - 1
            System.out.println();
            System.out.print("o");
            return;
        }

        while (N < Z) { //проверка: возможность построение гексагона с вводимым числом точек
            N = N + 6 * k; //Функция ряда для гексагона: N[k]=N[k-1]+6k, где: N[0]=1 и k>=1
            if (N < Z) { //Если N < Z, то перебор продолжится со следующим элементом k
                k++;
            } else if (N > Z) { //Если N превысит Z - построение невозможно
                System.out.print("\"Invalid\"");
                return;
            }
        } //В случае успешной проверки цикл завершится c N=Z

        //Построение гексагона
        System.out.println();
        int Px = 4 * k + 1; //Расчёт максимального количества символов в строке
        int Py = 2 * k + 1; //Расчёт максимального количества строк
        int new_k = k; //Изменяемый "k" для построения

        for (int y = 0; y < Py; y++, new_k--) { //Перебор всех строк при построении
            for (int x = 0; x < Px + 1; x++) { //Перебор всех символов при построении
                if (x < Math.abs(new_k)) { //Заполнение левых углов пробелами
                    System.out.print(" ");
                } else if ((Math.abs(new_k) <= x && x <= (Px - Math.abs(new_k) - 1)) && k % 2 == 0) {
                    //Заполнение середины самого гексагона если k чётный
                    if ((x + y) % 2 == 0) {
                        System.out.print("o");
                    } else {
                        System.out.print(" ");
                    }
                } else if ((Math.abs(new_k) <= x && x <= (Px - Math.abs(new_k) - 1)) && k % 2 != 0) {
                    //Заполнение середины самого гексагона если k нечётный
                    if ((x + y) % 2 == 0) {
                        System.out.print(" ");
                    } else {
                        System.out.print("o");
                    }
                } else if ((Px - Math.abs(new_k) - 1) < x && x < Px) {
                    System.out.print(" "); //Заполнение правых углов пробелами
                } else{
                    System.out.print("\n"); //Заполнение последнего элемента строки переносом
                }
            }
        }
    }

    public static void main(String[] args) {
        System.out.print("hexLattice(1) ➞ ");
        hexLattice(1);
        System.out.println();
        System.out.print("hexLattice(7) ➞ ");
        hexLattice(7);
        System.out.println();
        System.out.print("hexLattice(19) ➞ ");
        hexLattice(19);
        System.out.println();
        System.out.print("hexLattice(21) ➞ ");
        hexLattice(21);
    }
}
