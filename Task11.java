public class Task11 {
    public static String repeat(String string, int n){
        String res = ""; //строка-результат
        for (int i = 0; i < string.length(); i++){ //перебор вводимой строки
            char x = string.charAt(i); //назначение переменной символа согласно индексу из строки
            for (int j = 1; j <= n; j++) {
                res = res + x; //дублирование символа "n" раз в новой строке
            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println("mice\", 5) ➞ " + repeat("mice", 5));
        System.out.println("hello\", 3) ➞ " + repeat("hello", 3));
        System.out.println("stop\", 1) ➞ " + repeat("stop", 1));
    }
}
