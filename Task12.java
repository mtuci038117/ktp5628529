public class Task12 {
    public static int differenceMaxMin(int[] nums){
        int min = nums[0]; //минимальное значение по умолчанию - первый элемент массива
        int max = nums[0]; //максимальное значение по умолчанию - первый элемент массива
        for (int i = 1; i < nums.length; i++){ //перебор массива со второго значения
            if (min > nums[i]){ //поиск меньшего значения с присвоением
                min = nums[i];
            }
            if (max < nums[i]){ //поиск большего значения с присвоением
                max = nums[i];
            }
        }
        return Math.abs(max - min); //возврат расстояния между максимальным и минимальным значением
    }

    public static void main(String[] args) {
        System.out.println("differenceMaxMin([10, 4, 1, 4, -10, -50, 32, 21]) ➞ " + differenceMaxMin(new int[]{10, 4, 1, 4, -10, -50, 32, 21}));
        System.out.println("differenceMaxMin([44, 32, 86, 19]) ➞ " + differenceMaxMin(new int[]{44, 32, 86, 19}));
    }
}
