public class Task14 {
    public static void cumulativeSum(int[] nums){
        System.out.print("[" + nums[0]); //косметический вывод массива в виде строки с первым неизменяемым элементом
        for (int i = 1; i < nums.length; i++){ //перебор массива со второго элемента
            nums[i] = nums[i] + nums[i - 1]; //изменение значения текущего элемента массива на основе суммирования с предыдущим
            System.out.print(", " + nums[i]); //косметическое перечисление элементов в строке через запятую
        }
        System.out.print("]");
    }

    public static void main(String[] args) {
        System.out.print("cumulativeSum([1, 2, 3]) ➞ ");
        cumulativeSum(new int[]{1, 2, 3});
        System.out.println();
        System.out.print("cumulativeSum([1, -2, 3]) ➞ ");
        cumulativeSum(new int[]{1, -2, 3});
        System.out.println();
        System.out.print("cumulativeSum([3, 3, -2, 408, 3, 3]) ➞ ");
        cumulativeSum(new int[]{3, 3, -2, 408, 3, 3});
    }
}
