public class Task19 {
    public static String isPrefix(String word, String prefix){
        prefix = prefix.replace("-",""); //убрать "-" в префиксе
        if (word.startsWith(prefix)){ //проверка: слово начинается с префикса
            return "true";
        }
        else {
            return "false";
        }
        }

    public static String isSuffix(String word, String suffix){
        suffix = suffix.replace("-",""); //убрать "-" в суффиксе
        if (word.endsWith(suffix)){ //проверка: слово заканчиваетяс на суффикс
            return "true";
        }
        else {
            return "false";
        }
    }

    public static void main(String[] args) {
        System.out.println("isPrefix(\"automation\", \"auto-\") ➞ " + isPrefix("automation", "auto-"));
        System.out.println("isSuffix(\"arachnophobia\", \"-phobia\") ➞ " + isSuffix("arachnophobia", "-phobia"));
        System.out.println("isPrefix(\"retrospect\", \"sub-\") ➞ " + isPrefix("retrospect", "sub-"));
        System.out.println("isSuffix(\"vocation\", \"-logy\") ➞ " + isSuffix("vocation", "-logy"));
    }
}
