public class Task1 {
    public static int remainder(int a, int b){
        return a % b; //возврат остатка от деления
    }

    public static void main(String[] args) {
        System.out.println("remainder(1, 3) ➞ " + remainder(1,3));
        System.out.println("remainder(3, 4) ➞ " + remainder(3,4));
        System.out.println("remainder(-9, 45) ➞ " + remainder(-9,45));
        System.out.println("remainder(5, 5) ➞ " + remainder(5,5));
    }
}
