public class Task7 {
    public static int addUpTo(int x){
        int sum = 0;
        for (int i = 1; i <= x; i++){
            sum = sum + i; //Суммирование всех чисел от 1 до x
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println("addUpTo(3) ➞ " + addUpTo(3));
        System.out.println("addUpTo(10) ➞ " + addUpTo(10));
        System.out.println("addUpTo(7) ➞ " + addUpTo(7));
    }
}
