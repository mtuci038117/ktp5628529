import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task17 {
    public static String isValid(String index){
        Pattern p = Pattern.compile("[a-zA-Z]+"); //шаблон поиска всех букв из регулярного выражения
        Matcher m = p.matcher(index);
        //проверка: строка менее 6 цифр, без пробелов и букв
        if (index.length() <= 5 && index.indexOf(" ") == -1 && !m.find()){
            return "true";
        }
        else {
            return "false";
        }
        }

    public static void main(String[] args) {
        System.out.println("isValid(\"59001\") ➞ " + isValid("59001"));
        System.out.println("isValid(\"853a7\") ➞ " + isValid("853a7"));
        System.out.println("isValid(\"732 32\") ➞ " + isValid("732 32"));
        System.out.println("isValid(\"393939\") ➞ " + isValid("393939"));
    }
}
