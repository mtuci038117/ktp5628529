public class Task13 {
    public static String isAvgWhole(int[] nums){
        int sum = 0;
        for (int i = 0; i < nums.length; i++){ //перебор массива
            sum = sum + nums[i]; //суммирование всех элементов массива
        }
        if (sum % nums.length == 0){ //проверка: сумма элементов массива делится на их количество без остатка
            return "true";
        }
        else{
            return "false";
        }
    }

    public static void main(String[] args) {
        System.out.println("isAvgWhole([1, 3]) ➞ " + isAvgWhole(new int[]{1, 3}));
        System.out.println("isAvgWhole([1, 2, 3, 4]) ➞ " + isAvgWhole(new int[]{1, 2, 3, 4}));
        System.out.println("isAvgWhole([1, 5, 6]) ➞ " + isAvgWhole(new int[]{1, 5, 6}));
        System.out.println("isAvgWhole([1, 1, 1]) ➞ " + isAvgWhole(new int[]{1, 1, 1}));
        System.out.println("isAvgWhole([9, 2, 2, 5]) ➞ " + isAvgWhole(new int[]{9, 2, 2, 5}));
    }
}
