public class Task5 {
    public static String operation(int N, int a, int b){
        if (a + b == N){ //Получение числа N из суммы a и b
            return "added";
        }
        else if (a * b == N){ //Получение числа N из произведения a и b
            return "multiplied";
        }
        else if (a - b == N){ //Получение числа N из разности a и b
            return "subtracted";
        }
        else if (a / b == N){ //Получение числа N из деления a и b
            return "divided";
        }
        else{ //Нельзя получить число N
            return "none";
        }
    }

    public static void main(String[] args) {
        System.out.println("operation(24, 15, 9) ➞ " + operation(24, 15, 9));
        System.out.println("operation(24, 26, 2) ➞ " + operation(24, 26, 2));
        System.out.println("operation(15, 11, 11) ➞ " + operation(15, 11, 11));
    }
}
