public class Task16 {
    public static int Fibonacci(int num){
        int fib[] = new int[num + 2]; //создание массива Фибоначчи на основе вводимой длины +2 первых элемента
        fib[0] = 0; //первый постоянный элемент
        fib[1] = 1; //второй постоянный элемент
        for (int i = 2; i < num + 2; i++) { //перебор массива с третьего элемента
            fib[i] = fib[i - 1] + fib[i - 2]; //наполнение массива согласно функции Фибоначчи
        }
        return fib[num + 1]; //возврат последнего элемента массива
        }

    public static void main(String[] args) {
        System.out.println("Fibonacci(3) ➞ " + Fibonacci(3));
        System.out.println("Fibonacci(7) ➞ " + Fibonacci(7));
        System.out.println("Fibonacci(12) ➞ " + Fibonacci(12));
    }
}
