public class Task10 {
    public static String abcmath(int a, int b, int c){
        for (int i = 1; i <= b; i++){
            a = a * 2; //прибавление "a" к самому себе "b" раз
        }
        if (a % c == 0){ //просуммированное "a" делится без остатка на "c"
            return "true";
        }
        else {
            return "false";
        }
    }

    public static void main(String[] args) {
        System.out.println("abcmath(42, 5, 10) ➞ " + abcmath(42, 5, 10));
        System.out.println("abcmath(5, 2, 1) ➞ " + abcmath(5, 2, 1));
        System.out.println("abcmath(1, 2, 3) ➞ " + abcmath(1, 2, 3));
    }
}
